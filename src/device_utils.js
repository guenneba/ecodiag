
/* eslint-disable */

// import { i18n } from '@/plugins/i18n.js'
import * as Papa from 'papaparse'
import { devices, PARAMS, get_default_model, get_device_attribute, Device, is_empty_year, is_valid_year, is_valid, compute_status } from '@ecodiag/ecodiag-lib'

export const device_utils = {
  data () {
    return {
      status: {
        user_ko: 2,
        user_ok: 4,
        unknown: 6,
        unknown_year: 7,
        invalid_year: 8,
        csv_ok: 10
      },
      params: PARAMS
    }
  },

  methods: {

    clone_obj(x) {
      let res;
      res = Array.isArray(x) ? [] : {};
      for (let key in x) {
        let v = x[key]
        res[key] = (typeof v === "object") ? this.clone_obj(v) : v
      }
      return res;
    },

    // Get an element's label in locale language using:
    //  - obj.label_<local>, e.g., obj.label_fr or obj.label_en
    //  - or obj.label
    //  - or $t(obj) if obj is a string
    //  - or fallback
    tr_label (obj,fallback) {
      if (typeof obj === 'string') {
        return this.$i18n.t(obj)
      }
      let result = obj['label_'+this.$i18n.locale]
      if (!result)
        result = obj.label
      if (!result)
        result = fallback
      return result
    },

    capitalize (str) {
      return str[0].toUpperCase() + str.substring(1)
    },

    // Returns "type/model" unless model is null or "default" in which case we simply return type
    make_device_name (type, model) {
      if (model && model != '' && model != 'default') { return type + '/' + model }
      return type
    },

    // If name matches "type/model" then return ["type","model"]
    // otherwise check whether "name" is a valid device model,
    //    if so return ["type","name"]
    //    otherwise "name" is a 'type' and it returns ["name"]
    device_name_to_type_model (name) {
      let type_model = name.split('/')
      if (type_model.length == 2 || ((typeof purchase_types !== 'undefined') && type_model[0] in purchase_types)) { return type_model }
      // search within devices
      for (let k in devices) {
        let model = type_model[0]
        let d = devices[k]
        if (d.models && model in d.models) { return [k, model] }
      }

      return type_model
    },

    // Shortcut to get_device_attribute (obsolete)
    get_yearly_consumption (item) {
      return get_device_attribute(item.type, item.model, 'yearly_consumption')
    },

    // [obsolete]
    // Returns a copy of item with type as 'type', 'model' as model,
    // and other attributes copied from the input item if available,
    // or gathered from the database otherwise.
    consolidate_device_item (type, model, item) {
      const lifetime = item.lifetime ? item.lifetime : get_device_attribute(type, model, 'duration')
      let res = {
        type: type,
        model: model,
        nb: item.nb ? item.nb : item,
        lifetime: lifetime,
        lifetime2:          item.lifetime2          ? item.lifetime2          : lifetime * (+PARAMS.lifetime_factor),
        yearly_consumption: item.yearly_consumption ? item.yearly_consumption : get_device_attribute(type, model, 'yearly_consumption'),
        usage:              item.usage              ? item.usage              : get_device_attribute(type, model, 'usage')
      }

      return res
    },

    get_default_model (type) {
      // NOTE(ecodiag-lib): kept for backward compatibility for now
      return get_default_model(type)
    },

    is_empty_year(y) {
      // NOTE(ecodiag-lib): kept for backward compatibility for now
      return is_empty_year(y)
    },
    
    is_valid_year (y, method, ref_year) {
      // NOTE(ecodiag-lib): kept for backward compatibility for now
      return is_valid_year(y, method, ref_year)
    },

    compute_status: function (item, method, ref_year) {
      // NOTE(ecodiag-lib): kept for backward compatibility for now
      return compute_status(item, method, ref_year)
    },

    is_valid (item, method, ref_year) {
      // NOTE(ecodiag-lib): kept for backward compatibility for now
      return is_valid(item, method, ref_year)
    },

    extract_valid_items (items, method, ref_year) {
      let self = this
      return items.filter(e => self.is_valid(e, method, ref_year))
    },

    pack_device_list (d) {
      let res = []
      d.forEach(function (x) {
        let item = {}
        item[this.make_device_name(x.type, x.model)] = { nb: x.nb, lifetime: x.lifetime }
        res.push(item)
      }.bind(this))
      return res
    },

    unpack_device_list (d) {
      let res = []
      d.forEach(function (x) {
        let item_name = Object.keys(x)[0]
        let item_data = Object.values(x)[0]

        let type_model = this.device_name_to_type_model(item_name)
        let type = type_model[0]
        let model = 'default'

        if (type_model.length == 2) { model = type_model[1] }

        res.push(Device.fromType(type, {nb: item_data, model: model, score: 1}))
      }.bind(this))
      return res
    },

    // item is modified in-place and returned
    process_csv_raw_item_regex (item, header_map) {
      let score = 0

      item['score'] = 0
      item['type']  = ''
      item['model'] = ''

      const csvitem = item.csvdata

      for (let dev_key in devices) {
        const dev = devices[dev_key]
        let dev_score = 0
        let mod_score = 0
        if (dev.regex && header_map.in_type && csvitem[header_map.in_type]) {
          if (csvitem[header_map.in_type].search(dev.regex) >= 0) {
            dev_score = 1
            // console.log("found "+dev_key+" in "+csvitem[header_map.in_type]);
          }
        }
        if (dev_score >= score) {
          // otherwise we have no chance to find a better match

          if (dev_score > 0 && score == 0) {
            // we already found a better match
            score = dev_score
            item['score'] = score
            item['type']  = dev_key
            item['model'] = this.get_default_model(dev_key)
          }

          if (dev.models && header_map.in_model && csvitem[header_map.in_model]) {
            for (let m_key in dev.models) {
              const m = dev.models[m_key]
              if (m.regex) {
                if (csvitem[header_map.in_model].search(m.regex) >= 0) {
                  mod_score = 1
                  // console.log("found "+m_key+" in "+csvitem[header_map.in_model]);
                }
                if (dev_score + mod_score > score) {
                  score = dev_score + mod_score
                  item['score'] = score
                  item['type']  = dev_key
                  item['model'] = m_key
                  if (score == 2) { return item } else { break }
                }
              }
            }
          }
        }
      }

      return item
    },

    // Search for brand, type, model, and purchase year columns
    // within csv_data.columns, and return the matches as
    // {in_brand,in_type,in_model,in_date}
    csv_parse_headers (fields) {
      let find_header = function (regex) {
        for (let name of fields) {
          if (name.search(regex) >= 0) { return name }
        }
        return null
      }

      return {
        in_brand:     find_header(/(fabricant|brand|marque)/i),
        in_type:      find_header(/(type|cat.gorie|kind)/i),
        in_model:     find_header(/mod.le/i),
        in_date:      find_header(/date.*(achat|acquisition)/i),
        in_nb:        find_header(/(quantity|quantit.|nb)/i),
        in_lifetime:  find_header(/(lifetime|dur.e de vie)/i),
        in_tag:       find_header(/(tag)/i)
      }
    },

    // For each row i,
    //    extract year from:
    //      csv_data[i].csvdata[in_date]
    //    and store it in:
    //      csv_data[i].year
    //
    // NOTE: csv_data is modified in-place!
    csv_parse_dates (csv_data, in_date) {
      for (let item of csv_data) {
        item['year'] = ''
        if (in_date) {
          const date = item.csvdata[in_date]

          if (date) {
            const res = date.match(/\d{4}/)
            if (res && res.length > 0) { item['year'] = res[0] }
          }
        }
      }
    },

    // Merge rows of csv_data having the exact same key
    // and record the number of items within csv_data[i].nb
    csv_merge_wrt_keys (csv_data) {
      // sort items
      csv_data = csv_data.sort((a, b) => a.key.localeCompare(b.key))

      // insert while summing up duplicates
      let new_data = []
      for (let item of csv_data) {

        // skip empty lines
        if (item.key === '') {
          continue
        }
        
        if (!('nb' in item)) {
          item.nb = 1
        }
        if (new_data.length > 0 && new_data[new_data.length - 1].key == item.key) {
          new_data[new_data.length - 1].nb += item.nb
          new_data[new_data.length - 1].csvdata.lines = new_data[new_data.length - 1].csvdata.lines.concat(item.csvdata.lines)
        } else {
          new_data.push(item)
        }
      }
      if (csv_data.columns) { new_data.columns = csv_data.columns }
      if (csv_data.header_map) { new_data.header_map = csv_data.header_map }
      return new_data
    },

    // Merge rows of csv_data having the exact same year, brand, type and model,
    // and record the number of items within csv_data[i].nb
    csv_merge_raw_items (csv_data, header_map) {
      // create keys
      let ukey = 100000
      for (let i in csv_data) {
        let item = csv_data[i]
        let key = ''
        const csvitem = item.csvdata
        
        if (header_map.in_brand && csvitem[header_map.in_brand]) { key = key.concat(csvitem[header_map.in_brand]) }
        if (header_map.in_type && csvitem[header_map.in_type]) { key = key.concat(csvitem[header_map.in_type]) }
        if (header_map.in_model && csvitem[header_map.in_model]) { key = key.concat(csvitem[header_map.in_model]) }
        if (header_map.in_lifetime && csvitem[header_map.in_lifetime]) { key = key.concat(csvitem[header_map.in_lifetime]) }
        if (header_map.in_tag && csvitem[header_map.in_tag]) { key = key.concat(csvitem[header_map.in_tag]) }

        if (!this.is_empty_year(item['year'])) {
          key = key.concat(item['year'])
        } else if (header_map.in_date) {
          if (key !== '') {
            key = key.concat(ukey.toString())
            ukey += 1
          }
        }
        
        item['key'] = key
      }

      return this.csv_merge_wrt_keys(csv_data)
    },

    parse_raw_csv (data, meta, method) {
      let header_map = this.csv_parse_headers(meta.fields)

      for (let i in data) {
        let item = data[i]
        item['lines'] = [+i + 1]
        data[i] = {csvdata: item}
      }

      if (!(header_map.in_type || header_map.in_model)) {
        return [[], header_map, 'no type or model']
      }

      if (header_map.in_date) {
        this.csv_parse_dates(data, header_map.in_date)
      }
      if (method === 'flux' && header_map.in_lifetime) {
        header_map.in_lifetime = null
      }
      if (header_map.in_nb) {
        for (let item of data) {
          item.nb = parseInt(item.csvdata[header_map.in_nb])
        }
      }
      if (header_map.in_tag) {
        for (let item of data) {
          item.tag = item.csvdata[header_map.in_tag]
        }
      }
      data = this.csv_merge_raw_items(data, header_map)
      for (let i in data) {
        let item = data[i]
        item = this.process_csv_raw_item_regex(item, header_map)
        if (header_map.in_lifetime) {
          let lt = parseInt(item.csvdata[header_map.in_lifetime])
          if (!isNaN(lt)) {
            item.lifetime = lt
          }
        }
        data[i] = Device.fromType(item.type, item)
      }

      // sort according to scores
      data = data.sort((b, a) => a.score - b.score)

      return [data, header_map, 'ok']
    },

    parseFile (file, method) {
      let self = this
      return new Promise((resolve, reject) => {
        Papa.parse(file, {
          header: true,
          worker: true,
          delimitersToGuess: [',', '\t', ';'],
          skipEmptyLines: true,
          complete: function (result) {
            if (result.errors.length === 0) {
              resolve(self.parse_raw_csv(result.data, result.meta, method))
            } else {
              let error = result.errors[0]
              error.delimiter = result.meta.delimiter
              reject(error)
            }
          },
          error: function (error) {
            error.delimiter = null
            reject(error)
          }
        })
      })
    },

    toFixed(value, precision) {
      let power = Math.pow(10, precision || 0)
      return Math.round(value * power) / power
    }

  }

}
